package com.nomina.area.controller;

import com.nomina.area.model.entity.Area;
import com.nomina.area.model.service.IServiceArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ControllerArea {

    @Autowired
    private IServiceArea serviceArea;

    @GetMapping("/areas")
    @ResponseStatus(HttpStatus.FOUND)
    public List<Area> listarAreas() throws Exception{
        return (List<Area>)this.serviceArea.findByAll();
    }

    @GetMapping("/area/{id}")
    @ResponseStatus(HttpStatus.FOUND)
    public Area listarPorId(@PathVariable long id) throws Exception    {
        return this.serviceArea.findById(id);
    }

    @GetMapping("/area/codigo/{id}")
    @ResponseStatus(HttpStatus.FOUND)
    public Area listarPorCodigo(@PathVariable String id) throws Exception{
        return this.serviceArea.findByCodigo(id);
    }



    @PostMapping("/area/crear")
    @ResponseStatus(HttpStatus.CREATED)
    public Area crear(@RequestBody  Area area)throws Exception{
        return this.serviceArea.save(area);
    }

    @DeleteMapping("/area/borrar/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrar(@PathVariable long id){
        this.serviceArea.deleteById(id);
    }

    @PutMapping("/area/editar/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Area editar (@PathVariable long id, @RequestBody Area area) throws Exception{
       return this.serviceArea.editById(id,area);
    }

    @PutMapping("/estadoArea/{id}/estado/{estado}")
    public Area editarEstado(@PathVariable long id, @PathVariable String estado, @RequestBody Area area )throws Exception
    {
        return this.serviceArea.editByEstado(id, estado,area);
    }
}
