package com.nomina.area.model.repository;

import com.nomina.area.model.entity.Area;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAreaDao extends CrudRepository<Area,Long> {
   public Area findByCodigo(String codigo);
   public Area findByIdAndEstado(long id, boolean estado);

}
