package com.nomina.area.model.service.implement;

import com.nomina.area.model.entity.Area;
import com.nomina.area.model.repository.IAreaDao;
import com.nomina.area.model.service.IServiceArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class ServiceArea implements IServiceArea {

    @Autowired
    private IAreaDao areaDao;

    @Override
    @Transactional
    public Area save(Area area) throws Exception{
            if(area==null || area.getCodigo().isEmpty() || area.getDescripcion().isEmpty()){
                throw new Exception("Datos invalidos");
            }
        return this.areaDao.save(area);

    }

    @Override
    @Transactional
    public void deleteById(long id) {


        this.areaDao.deleteById(id);

    }

    @Override
    @Transactional(readOnly = true)
    public List<Area> findByAll() {

        return (List<Area>) this.areaDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Area findById(long id) throws Exception{
        Area area=this.areaDao.findById(id).orElse(null);
        if(area==null){
            throw new Exception("Area no existe");
        }
        return this.areaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Area editById(long id, Area area) throws Exception{
        Area areaNueva=this.areaDao.findById(id).orElse(null);
        if(areaNueva==null){
            throw new Exception("Area no existe");
        }
        areaNueva.setCodigo(area.getCodigo());
        areaNueva.setDescripcion(area.getDescripcion());

        return this.areaDao.save(areaNueva);
    }

    @Override
    @Transactional(readOnly = true)
    public Area findByCodigo(String codigo) throws Exception{
            if(this.areaDao.findByCodigo(codigo)==null){
                throw new  Exception("Area no existe");
            }
        return this.areaDao.findByCodigo(codigo);
    }

    @Override
    @Transactional
    public Area editByEstado(long id,  String estado, Area area) throws Exception{
       // area=this.areaDao.findById(id).orElse(null);

        Area nueva=this.areaDao.findByIdAndEstado(id, Boolean.valueOf(estado));
        if(nueva==null){
            throw new Exception("area no encontrada");
        }
        nueva.setEstado(area.isEstado());
        return nueva;
    }


}
