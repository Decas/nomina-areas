package com.nomina.area.model.service;

import com.nomina.area.model.entity.Area;

import java.util.List;

public interface IServiceArea {

    public List<Area> findByAll()throws Exception;

    public Area save(Area  area) throws Exception;

    public Area findById(long id)throws Exception;

    public void deleteById(long id);

    public Area editById(long l, Area area)throws Exception;

    public Area findByCodigo(String codigo)throws Exception;

    public Area editByEstado(long id, String estado,Area area) throws Exception;

}
