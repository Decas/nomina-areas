package com.nomina.area.model.entity;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "areas")
@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Area implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;

    @Column(name = "codigo",nullable = false, length = 20)
private String codigo;
    @Column(name = "descripcion",nullable = false, length = 80)
private String descripcion;
    @Column(name = "estado")
private boolean estado;


}
