package com.nomina.area.model.service;

import com.nomina.area.model.entity.Area;
import com.nomina.area.model.repository.IAreaDao;
import com.nomina.area.model.service.implement.ServiceArea;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ServiceAreaTest {

    @Mock
    private Area area;

    @Mock
    private Area area2;

    @Mock
    private IAreaDao areaDao;

    @InjectMocks
    private ServiceArea serviceArea;

    @Mock
    private List<Area> areaList;

    @Before
  public void setUp(){
        area=new Area(1L,"calidad","asdasd", true);
        area2=new Area(2L,"desarrollo","asdasd", true);
    areaList=new ArrayList<Area>();//inicializar siempre
        areaList.add(area);
        areaList.add(area2);


    }
    @Test
    @DisplayName("Test search all Success")
   public void listarAreasTest()throws Exception{
        when(areaDao.findAll()).thenReturn(this.areaList);
        //verifica que el tamaño de la lista sea igual
        Assertions.assertThat(2).isEqualTo(this.serviceArea.findByAll().size());

    }
    @Test
    public void findByCodigoTest() throws Exception{
        when(areaDao.findByCodigo("calidad")).thenReturn(area);
        Assertions.assertThat(area).isEqualTo(this.serviceArea.findByCodigo("calidad"));
    }

    @Test
    @DisplayName("Test id serch Success")
    public void findByIdTest() throws Exception{
        //retorna el objeto con el respectivo id
        when(areaDao.findById(1L)).thenReturn(Optional.ofNullable(area));
       //verifica que los objetos con el mismo id sean iguales
       Assertions.assertThat(area).isEqualTo(this.serviceArea.findById(1L));

    }

    @Test
    @DisplayName("Test save Success")
    public void saveTest() throws Exception{
        //retorna el objeto nuevo
        when(this.areaDao.save(area2)).thenReturn(area2);
        //verifica que los objetos son iguales
        Assertions.assertThat(area2).isEqualTo(this.serviceArea.save(area2));

    }

    @Test
    @DisplayName("Test delete Success")
    public void deleteTest() throws Exception{
        this.serviceArea.deleteById(1L);
        //verica que se ejecuta 1 vez
        //dado que no hay id repetidos tiene que ejecutarse 1 por id registrado
        verify(this.areaDao,times(1)).deleteById(1L);
            }


     @Test
     @DisplayName("Test edit Success")
     public void editTest() throws Exception{
        //  retorna un area a un objetonullable
        lenient().when(this.areaDao.findById(2L)).thenReturn(Optional.ofNullable(area2));

        area2.setDescripcion("descrop");
        area2.setCodigo("pruebas");
        this.serviceArea.editById(2L,area2);
     }

     @Test
    public void editEstadoTest() throws Exception{
         lenient().when(this.areaDao.findByIdAndEstado(2L,true)).thenReturn(area2);
         area2.setEstado(true);
         this.serviceArea.editByEstado(2L,"true",area2);
     }


}
